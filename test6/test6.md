# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

学号：202010414212，姓名：欧阳廷浩，班级：软工2班

设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。

- 文档

  ```
  必须提交
  ```

  到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：

  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx]()，学校格式的完整报告。

- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。

- 提交时间： 2023-5-26日前

## 评分标准

| 评分项     | 评分标准                             | 满分 |
| ---------- | ------------------------------------ | ---- |
| 文档整体   | 文档内容详实、规范，美观大方         | 10   |
| 表设计     | 表设计及表空间设计合理，样例数据合理 | 20   |
| 用户管理   | 权限及用户分配方案设计正确           | 20   |
| PL/SQL设计 | 存储过程和函数设计正确               | 30   |
| 备份方案   | 备份方案设计正确                     | 20   |

## 实验内容

### 表空间设计方案：

1. 创建表空间1（TBS1）：用于存放用户数据表和索引。
2. 创建表空间2（TBS2）：用于存放系统表和索引。

​                              ![p1](p1.png)

 

 ![p2](p2.png)

### 表设计方案：

 

表1：商品信息表（Products）

字段：商品ID（ProductID，主键）、商品名称（ProductName）、商品描述（ProductDescription）、价格（Price）、库存（Stock）等。

 ![p3](p3.png)

表2：客户信息表（Customers）

字段：客户ID（CustomerID，主键）、客户名称（CustomerName）、联系地址（Address）、联系电话（PhoneNumber）等。

 ![p4](p4.png)

表3：订单信息表（Orders）

字段：订单ID（OrderID，主键）、客户ID（CustomerID，外键关联Customers表）、下单日期（OrderDate）、总金额（TotalAmount）等。

 ![p5](p5.png)

表4：订单明细表（OrderDetails）

字段：订单明细ID（DetailID，主键）、订单ID（OrderID，外键关联Orders表）、商品ID（ProductID，外键关联Products表）、数量（Quantity）、单价（UnitPrice）等。

 ![p6](p6.png)

 

 所有表已创建

 ![p7](D:\oracle\test6\p7.png)

### 权限及用户分配方案：

 

创建用户1（USER1）：

 

分配表空间：TBS1

授予对Products表和Customers表的SELECT、INSERT、UPDATE、DELETE权限。

创建用户2（USER2）：

 

分配表空间：TBS2

授予对Orders表和OrderDetails表的SELECT、INSERT、UPDATE、DELETE权限。

 ```
 -- 创建用户1并分配权限 CREATE USER USER1 IDENTIFIED BY password DEFAULT TABLESPACE TBS1; GRANT CONNECT, RESOURCE TO USER1; GRANT SELECT, INSERT, UPDATE, DELETE ON Products TO USER1; GRANT SELECT, INSERT, UPDATE, DELETE ON Customers TO USER1;
 
 -- 创建用户2并分配权限 CREATE USER USER2 IDENTIFIED BY password DEFAULT TABLESPACE TBS2; GRANT CONNECT, RESOURCE TO USER2; GRANT SELECT, INSERT, UPDATE, DELETE ON Orders TO USER2; GRANT SELECT, INSERT, UPDATE, DELETE ON OrderDetails TO USER2;
 ```



### 程序包设计方案：

在数据库中创建一个程序包（Package），使用PL/SQL语言设计存储过程和函数，实现复杂的业务逻辑。

 

例如，创建一个程序包名为SalesPackage，包含以下存储过程和函数：

 

存储过程1：创建订单（CreateOrder）

 

输入参数：客户ID、商品ID、数量

功能：创建新的订单并更新库存数量

存储过程2：取消订单（CancelOrder）

 

输入参数：订单ID

功能：取消指定订单，并将对应商品的库存数量恢复

函数1：计算订单总金额（CalculateTotalAmount）

 

输入参数：订单ID

返回值：订单的总金额

 ![p7](p8.png)

 

 ![p9](p9.png)

备份方案： 设计一套数据库的备份方案是确保数据安全和可恢复性的重要措施。以下是一个简单的备份方案：

1. 定期完全备份（Full Backup）：定期执行完全备份，将整个数据库备份到磁盘或磁带中。建议每周执行一次完全备份
2. 增量备份（Incremental Backup）：在完全备份之后，每天执行增量备份，只备份自上次备份以来发生更改的数据和日志文件。这样可以减少备份时间和存储空间的需求。
3. 日志备份（Log Backup）：定期备份数据库的事务日志文件，以便在发生故障时进行恢复。可以按小时或每天执行日志备份，以根据系统的需求和事务活动来决定备份频率。
4. 数据库备份验证：定期进行备份验证，确保备份文件的完整性和可恢复性。通过还原备份到测试环境，并执行一些测试操作，验证备份文件的有效性。
5. 存储备份文件：备份文件应存储在安全的位置，最好是远程位置或离原始数据库服务器足够远的位置，以保护备份免受物理损坏或灾难性事件的影响。
6. 定期恢复测试：定期进行恢复测试，验证备份的可用性和恢复时间。通过恢复数据库到测试环境并验证数据的完整性，以确保在实际灾难发生时能够迅速恢复数据库。
7. 定期更新备份策略：根据业务需求和数据增长情况，定期评估和更新备份策略。确保备份频率和存储容量能够满足业务需求，并适应数据库的增长和变化。

 

```
-- 执行完全备份 RMAN> BACKUP DATABASE;

-- 执行增量备份 RMAN> BACKUP INCREMENTAL LEVEL 1 DATABASE;

-- 执行日志备份 RMAN> BACKUP ARCHIVELOG ALL;

-- 还原数据库到指定时间点 RMAN> RUN { SET UNTIL TIME 'YYYY-MM-DD HH24:MI:SS'; RESTORE DATABASE; RECOVER DATABASE; }
```

## 实验总结

在本次实验中，我设计了一个基于Oracle数据库的商品销售系统的数据库设计方案，并实现了表及表空间设计、权限及用户分配、存储过程和函数的设计，以及数据库备份方案。通过这次实验,使我学到了不少实用的知识,更重要的是,做实验的过程,思考问题的方法,这与做其他的实验是通用的,真正使我们受益匪浅
