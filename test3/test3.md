# 实验3：创建分区表

学号：202010414212，姓名：欧阳廷浩，班级：软工2班

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验步骤

用户sale 连接数据库

![p3](p3.png)

- 使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- orders表按订单日期（order_date）设置范围分区。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);

```

> ![p1](p1.png)
>



```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```

![p2](p2.png)



- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。

```sql
CREATE INDEX orders_customer_name_idx ON orders (customer_name) TABLESPACE USERS;

```



- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;

CREATE SEQUENCE  SEQ2  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;

```



- order_details表设置引用分区。。

```sql
ALTER TABLE order_details
ADD CONSTRAINT order_details_fk1
FOREIGN KEY (order_id)
REFERENCES orders (order_id)
ON DELETE CASCADE
ON UPDATE CASCADE
ENABLE
PARTITION BY REFERENCE (order_details_fk1);


```

> - 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
>
> ```sql
> -- 插入订单数据，按照不同的日期分别插入到不同分区中
> INSERT INTO orders (order_id, customer_name, customer_tel, order_date, employee_id, discount, trade_receivable)
> SELECT SEQ1.nextval, 'Customer '||level, 'Tel '||level, 
>        to_date('2021-01-01','YYYY-MM-DD')+trunc(dbms_random.value(1,365)) as order_date, 
>        mod(level,1000)+1 as employee_id, 
>        trunc(dbms_random.value(1,10000))/100 as discount, 
>        trunc(dbms_random.value(1,100000))/100 as trade_receivable
> FROM dual 
> CONNECT BY level<=400000;
> 
> -- 插入订单详情数据，按照对应的订单ID插入到对应的分区中
> INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
> SELECT SEQ2.nextval, mod(level,400000)+1 as order_id, 'Product '||mod(level,100)+1, 
>        trunc(dbms_random.value(1,100))/100, 
>        trunc(dbms_random.value(1,100))/100
> FROM dual 
> CONNECT BY level<=2000000;
> 
> 
> 
> ```
>
> > 

- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。

```sql
#插入数据的脚本可以按以下格式编写：
INSERT INTO orders (order_id, customer_name, customer_tel, order_date, employee_id, discount, trade_receivable)
SELECT orders_seq.nextval, 'customer_name', 'customer_tel', SYSDATE, TRUNC(DBMS_RANDOM.VALUE(1, 10)), 0, 0
FROM dual
CONNECT BY LEVEL <= 400000;

-- order_details 表插入数据
INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
SELECT order_details_seq.nextval, o.order_id, 'product_id', 5, 10
FROM orders o
WHERE ROWNUM <= 2000000;

#联合查询的语句
SELECT o.order_id, o.customer_name, od.product_id, od.product_num, od.product_price
FROM orders o
JOIN order_details od
ON o.order_id = od.order_id
WHERE o.order_date BETWEEN TO_DATE('2016-01-01', 'YYYY-MM-DD') AND TO_DATE('2023-05-06', 'YYYY-MM-DD');



```

> 



## 总结

通过这次实验,使我学到了不少实用的知识,更重要的是,做实验的过程,思考问题的方法,这与做其他的实验是通用的,真正使我们受益匪浅
